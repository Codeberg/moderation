package main

import (
	"flag"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"code.gitea.io/sdk/gitea"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/jwtauth/v5"
	"github.com/go-chi/render"
	"github.com/go-ini/ini"

	"codeberg.org/codeberg/moderation/internal/auth"
	"codeberg.org/codeberg/moderation/internal/log"
	"codeberg.org/codeberg/moderation/internal/mailer"
)

/* order of file headers
 * - imports
 * - globals, consts
 * - type definitions
 * - routes
 * - config function
 */

/* common globals
 * globals that are only accessed in a single file are defined there
 * using a namespace alike filenameConstantname
 */

var (
	giteaURL    string
	giteaClient *gitea.Client
)

type GenericRequest struct {
	ExpandRepoActionToForks    bool   `json:"expandforks"`
	InformUser                 bool   `json:"informuser"`
	InformUserAdditionalReason string `json:"informuserreason"`
	InternalActionComment      string `json:"internalcomment"`
}

func main() {
	// configuration
	cfg, err := ini.Load("moderation_backend.ini")
	if err != nil {
		log.Fatal("ini.Load: %v", err)
	}

	appListenUrl := cfg.Section("moderation_app").Key("LISTEN_URL").String()
	if appListenUrl == "" {
		log.Fatal("Please make sure you have a listen url in your moderation_backend.ini")
	}

	giteaToken := cfg.Section("gitea").Key("API_TOKEN").String()
	giteaURL = cfg.Section("gitea").Key("URL").String()
	if giteaToken == "" || giteaURL == "" {
		log.Fatal("Please make sure you have token and url in your moderation_backend.ini")
	}

	mailer.Config(cfg)
	userConfig(cfg)
	repoConfig(cfg)
	log.Config(cfg)
	auth.Config(cfg)

	// inits
	giteaClient, err = gitea.NewClient(giteaURL, gitea.SetToken(giteaToken))
	if err != nil {
		log.Fatal("Could not connect[url=%q]: %v", giteaURL, err)
	}

	// chi routing etc
	flag.Parse()

	r := chi.NewRouter()

	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(jwtauth.Verifier(auth.JwtAuth))

	r.Get("/authorize", auth.Authorize(cfg))
	r.Get("/oauth", auth.OauthCallback(cfg))
	r.Get("/user", auth.UserHandler)

	r.Route("/users", userRoutes)
	r.Route("/repos", repoRoutes)
	r.Group(auth.Basicauth) // makes sure that fetching an API token is protected by Basicauth

	workDir, _ := os.Getwd()
	filesDir := http.Dir(filepath.Join(workDir, "../frontend/dist"))

	r.Get("/*", func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(filesDir))
		fs.ServeHTTP(w, r)
	})

	log.Info("Starting app now on %s", appListenUrl)
	log.Fatal("http: %v", http.ListenAndServe(appListenUrl, r))
}

func filterInto(source, targetStruct interface{}) (interface{}, error) {
	sourceSlice := reflect.ValueOf(source)
	targetType := reflect.TypeOf(targetStruct)
	targetVal := reflect.ValueOf(targetStruct)
	if sourceSlice.Kind() != reflect.Ptr || targetType.Kind() != reflect.Ptr {
		return nil, log.NewError("filterInto: No pointer")
	}
	sourceSlice = sourceSlice.Elem()
	targetType = targetType.Elem()
	targetVal = targetVal.Elem()
	if sourceSlice.Kind() != reflect.Slice || targetType.Kind() != reflect.Struct || targetVal.Kind() != reflect.Struct {
		return nil, log.NewError("filterInto: No slice")
	}
	if sourceSlice.Len() == 0 {
		return nil, nil
	}

	var targetFields []reflect.StructField
	for i := 0; i < targetType.NumField(); i++ {
		targetFields = append(targetFields, targetType.Field(i))
	}
	var targetSlice []interface{}
	for i := 0; i < sourceSlice.Len(); i++ {
		newStruct := reflect.New(targetType)
		for j := range targetFields {
			structfield := newStruct.Elem().FieldByName(targetFields[j].Name)
			structfield.Set(sourceSlice.Index(i).Elem().FieldByName(targetFields[j].Name))
		}
		targetSlice = append(targetSlice, newStruct.Interface())
	}
	return targetSlice, nil
}
