package database

import (
	"time"
)

type ActionType int

const (
	ACTION_USER_TEMPBAN ActionType = iota + 1
	ACTION_USER_UNBAN
	ACTION_USER_PERMABAN
	ACTION_USER_REMOVE
)

type Action struct {
	ID        uint `gorm:"primaryKey"`
	Type      ActionType
	Timestamp time.Time
	Moderator string
	Comment   string
	Scheduled bool
}

func ActionNew(action *Action) error {
	result := db.Create(&action)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

type User struct {
	ID          uint `gorm:"primaryKey"`
	GiteaUserID int64
	ActionID    uint
	Action      Action
	UserName    string
	FullName    string
	Email       string
	LastLogin   time.Time
	Location    string
	Description string
}

func LogUserAction(user *User, action *Action) error {
	result := db.Create(&action)
	if result.Error != nil {
		return result.Error
	}

	user.ActionID = action.ID
	result = db.Create(&user)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func LogUserRemove(user *User, action *Action) error {
	action.Type = ACTION_USER_REMOVE
	err := LogUserAction(user, action)
	if err != nil {
		return err
	}
	return nil
}
