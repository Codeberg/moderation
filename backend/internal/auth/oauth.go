package auth

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"code.gitea.io/sdk/gitea"
	"github.com/go-ini/ini"
)

var storedOAuthStates map[string]struct{}

type TokenRequest struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	Code         string `json:"code"`
	GrantType    string `json:"grant_type"`
	RedirectUri  string `json:"redirect_uri"`
}

type TokenResponse struct {
	AccessToken string `json:"access_token"`
}

// randomBytes will return n bytes, each byte can be between [0, 255].
func randomBytes(n int) ([]byte, error) {
	bytes := make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		return nil, err
	}

	return bytes, nil
}

func Authorize(cfg *ini.File) func(http.ResponseWriter, *http.Request) {
	oauthconfig := cfg.Section("oauth")
	clientId := oauthconfig.Key("CLIENT_ID").String()
	redirectUri := oauthconfig.Key("REDIRECT_URI").String()

	if clientId == "" || redirectUri == "" {
		return func(w http.ResponseWriter, r *http.Request) {
			_, _ = w.Write([]byte("oauth not properly configured, check the ini file"))
			w.WriteHeader(http.StatusInternalServerError)
		}
	} else {
		return func(w http.ResponseWriter, r *http.Request) {
			rBytes, err := randomBytes(16)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			state := hex.EncodeToString(rBytes)

			storedOAuthStates[state] = struct{}{}
			oauthRedirect := fmt.Sprintf("%s/login/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&state=%s", giteaURL, clientId, redirectUri, state)

			w.Header().Set("Location", oauthRedirect)
			w.WriteHeader(http.StatusFound)
		}
	}
}

func UserHandler(w http.ResponseWriter, r *http.Request) {
	token, err := VerifyRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		_, _ = w.Write([]byte("invalid token"))
	} else {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte(token.Subject()))
	}
}

func getUserFromGitea(accessToken string) *gitea.User {
	userClient, _ := gitea.NewClient(giteaURL, gitea.SetToken(accessToken))
	user, _, _ := userClient.GetMyUserInfo()
	return user
}

func OauthCallback(cfg *ini.File) func(http.ResponseWriter, *http.Request) {
	oauthconfig := cfg.Section("oauth")
	clientId := oauthconfig.Key("CLIENT_ID").String()
	clientSecret := oauthconfig.Key("CLIENT_SECRET").String()
	redirectUri := oauthconfig.Key("REDIRECT_URI").String()

	return func(w http.ResponseWriter, r *http.Request) {
		code := r.URL.Query().Get("code")
		state := r.URL.Query().Get("state")

		if _, ok := storedOAuthStates[state]; !ok {
			_, _ = w.Write([]byte("invalid state"))
			w.WriteHeader(http.StatusConflict)
			return
		}
		delete(storedOAuthStates, state)

		tokenUrl := fmt.Sprintf("%s/login/oauth/access_token", giteaURL)

		tokenRequest := TokenRequest{
			clientId,
			clientSecret,
			code,
			"authorization_code",
			redirectUri,
		}

		tokenRequestJson, _ := json.Marshal(tokenRequest)
		tokenResponse, _ := http.Post(tokenUrl, "application/json", bytes.NewBuffer(tokenRequestJson))

		var tokenResponseJson TokenResponse
		_ = json.NewDecoder(tokenResponse.Body).Decode(&tokenResponseJson)

		user := getUserFromGitea(tokenResponseJson.AccessToken)

		if user.IsAdmin {
			expiration := time.Now().Add(3 * 24 * time.Hour)
			_, newToken, _ := JwtAuth.Encode(map[string]interface{}{"p": PERMISSION_ADMIN, "login": user.UserName, "exp": expiration.UTC().Unix()})

			cookie := http.Cookie{Name: "jwt", Value: newToken, Expires: expiration, HttpOnly: true, Secure: true, Path: "/"}
			http.SetCookie(w, &cookie)
			w.Header().Set("Location", "/?message=login_success")
			w.WriteHeader(http.StatusFound)
		} else {
			w.Header().Set("Location", "/?message=login_forbidden")
			w.WriteHeader(http.StatusFound)
		}
	}
}
