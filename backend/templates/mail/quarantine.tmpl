Hello {{.toName}},

your email address is registered at Codeberg.org, a non-profit platform for Free Software and Content development.
Unlike larger, proprietary platforms, Codeberg is fully powered by donations of people like you, who believe that free content should be hosted on free platforms.
With your trust and support, we feel honoured, and we try to make sure that the donations we receive unfold a maximum impact and benefit the Free Software and content movement.
For this, we have to ensure that all content on Codeberg.org is free to use and is properly licenced under an FSF-/ OSI approved Free Software licence. Also, we ensure fair use of our costly ressources like storage, traffic and bandwidth and might ask you to reduce this if we find it unreasonable for certain use-cases.

Regarding free software and licencing, please also read our documentation about https://docs.codeberg.org/getting-started/licensing/ or learn more about Free Software at https://www.fsf.org/about/what-is-free-software#.


Our moderation team has noticed that your content on Codeberg.org:

- {{.giteaURL}}/{{.repoOwner}}/{{.repoName}}

does not seem to be in line with our mission or rules, and we are afraid we can't continue providing service for your content in the current form.

{{if not (eq .reason "")}}
The moderation team has left a comment:
{{.reason}}

{{end}}
Please make sure your content respects the Terms of Use. Personal cloud mirrors or media backup services are explicitly not provided by Codeberg.org.

Please resolve the situation as soon as possible. Feel free to contact us if you believe this is a mistake.

Kind regards,
Your Codeberg e.V.

--
https://codeberg.org
Codeberg e.V.  –  Gormannstraße 14  –  10119 Berlin  –  Germany
Registered at registration court Amtsgericht Charlottenburg VR36929.
