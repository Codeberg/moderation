package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"code.gitea.io/sdk/gitea"

	"github.com/go-chi/chi/v5"
	"github.com/go-ini/ini"

	"codeberg.org/codeberg/moderation/internal/auth"
	"codeberg.org/codeberg/moderation/internal/log"
	"codeberg.org/codeberg/moderation/internal/mailer"
	"codeberg.org/codeberg/moderation/internal/middleware"
)

var reposQuarantineNewOwner string

type RepoData struct {
	Owner string `json:"owner"`
	Name  string `json:"name"`
}

type RepoRequest struct {
	GenericRequest
	List []RepoData `json:"list"`
}

func repoRoutes(r chi.Router) {
	r.Post("/quarantine", middleware.Run(auth.PERMISSION_MODERATOR, repoQuarantine))
	r.Post("/inform", middleware.Run(auth.PERMISSION_MODERATOR, repoInformOwner))
}

func repoConfig(cfg *ini.File) {
	repoconfig := cfg.Section("repos")
	reposQuarantineNewOwner = repoconfig.Key("QUARANTINE_OWNER").String()
}

func repoTransfer(currentOwner, currentName, newOwner, newName string) error {
	if currentOwner == "" || currentName == "" || newOwner == "" || newName == "" {
		return log.NewError("repoTransfer: some empty value passed")
	}

	_, _, err := giteaClient.TransferRepo(currentOwner, currentName, gitea.TransferRepoOption{
		NewOwner: newOwner,
	})
	if err != nil {
		return err
	}

	_, _, err = giteaClient.EditRepo(newOwner, currentName, gitea.EditRepoOption{
		Name: &newName,
	})
	if err != nil {
		return err
	}
	return nil
}

func repoQuarantine(w http.ResponseWriter, r *http.Request) error {
	request := RepoRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return log.NewError("Error decoding request's body as JSON: %v", err)
	}

	for _, repodata := range request.List {
		giteaUser, _, err := giteaClient.GetUserInfo(repodata.Owner)
		if err != nil {
			return log.NewError("Not able to GetUserInfo[owner=%s]: %v", repodata.Owner, err)
		}

		// Make the new name in the format of: unixTime_oldUserID_oldUserName_oldRepoName
		newName := fmt.Sprintf("%d_%d_%s_%s", time.Now().Unix(), giteaUser.ID, repodata.Owner, repodata.Name)
		if err = repoTransfer(repodata.Owner, repodata.Name, reposQuarantineNewOwner, newName); err != nil {
			return log.NewError("repoTransfer[old_owner=%s old_repo_name=%s new_owner=%s new_repo_name=%s]: %v", repodata.Owner, repodata.Name, reposQuarantineNewOwner, newName, err)
		}

		mailTemplateData := make(map[string]interface{})
		mailTemplateData["repoName"] = repodata.Name
		mailTemplateData["repoOwner"] = repodata.Owner
		if request.InformUser {
			mailTemplateData["reason"] = request.InformUserAdditionalReason
		}
		if err = mailer.MailSend(giteaClient, giteaUser, "quarantine", mailTemplateData); err != nil {
			return log.NewError("MailSend[email=%q, owner=%q]: %v", giteaUser.Email, repodata.Owner, err)
		}
	}
	return nil
}

func repoInformOwner(w http.ResponseWriter, r *http.Request) error {
	request := RepoRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return log.NewError("Error decoding request's body as JSON: %v", err)
	}

	for _, repodata := range request.List {
		giteaUser, _, err := giteaClient.GetUserInfo(repodata.Owner)
		if err != nil {
			return log.NewError("Not able to GetUserInfo[owner=%s]: %v", repodata.Owner, err)
		}

		mailTemplateData := make(map[string]interface{})
		mailTemplateData["repoName"] = repodata.Name
		mailTemplateData["repoOwner"] = repodata.Owner
		if request.InformUser {
			mailTemplateData["reason"] = request.InformUserAdditionalReason
		}

		if err := mailer.MailSend(giteaClient, giteaUser, "quarantine", mailTemplateData); err != nil {
			return log.NewError("MailSend[email=%q, owner=%q]: %v", giteaUser.Email, repodata.Owner, err)
		}
	}
	return nil
}
