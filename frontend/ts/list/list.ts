import { lists } from '../main';
import { render, renderAll } from '../render';
import { copyRow } from './row';

/**
 * This function creates a new list.
 * @param index Index where the list will be created. If undefined the list will be appended.
 * @param title Title of the list. Leave empty for list without title.
 */
export function createList (index: number = lists.length, title?: string) {
	// create a render target
	const renderTarget = document.createElement('div');
	renderTarget.id = `cb-list-output-${ lists.length }`;
	document.getElementById('cb-list-output')!.append(renderTarget);
	// create list element
	lists.push({
		title: title,
		type: undefined,
		listRows: [],
		renderTarget: renderTarget,
		generatedBy: []
	});

	// copy all lists to the next until the list with the index is empty if the list was not appended
	if (lists.length - 1 !== index) {
		let copyIndex = lists.length - 1;
		do {
			copyIndex--;
			lists[copyIndex + 1].listRows = lists[copyIndex].listRows;
			lists[copyIndex + 1].type = lists[copyIndex].type;
			lists[copyIndex + 1].title = lists[copyIndex].title;
			lists[copyIndex + 1].generatedBy = lists[copyIndex].generatedBy;
			render(copyIndex + 1);
		} while (copyIndex !== index);
		lists[copyIndex].listRows = [];
		lists[copyIndex].type = undefined;
		lists[copyIndex].generatedBy = [];
		lists[copyIndex].title = title;

		console.log(title === undefined);
	}

	if (title === undefined) render(index);
	else renderAll();
}

/**
 * This function copies a list to another list in the array.
 * @param source Index of the list to copy.
 * @param to Title of the list to copy to. Leave empty to copy to the next list.
 */
export function copyList (source: number, to?: string) {
	// copy all rows to the next list
	for (const rowIndex in lists[source].listRows) {
		copyRow(source, Number(rowIndex), to);
	}

	// render the next list
	render(source + 1);
}

/**
 * This function clears the content of a list without deleting it.
 * @param index Index of the list to clear.
 */
export function clearList (index: number) {
	// unset all list data
	lists[index].listRows = [];
	lists[index].type = undefined;
	lists[index].generatedBy = [];
	render(index);
}

/**
 * This function removes a list from the lists array.
 * @param index Index of the list to remove.
 */
export function dropList (index: number) {
	// move all lists after the lists that will be dropped to their previous list index
	for (let i = index; i < lists.length - 1; i++) {
		lists[i] = lists[i + 1];
		lists[i].renderTarget = document.getElementById(`cb-list-output-${ i }`)!;
	}
	// drop the last list and remove the render target
	lists.pop();
	document.getElementById(`cb-list-output-${ lists.length }`)!.remove();
	renderAll();
}

/**
 * This function adds the generatedBy tag to a list while checking for duplicate entries.
 * @param index Index of the list where the generator should be added.
 * @param generator String of the generator which can be used by the user to identify source for the list and why the list is displayed.
 */
export function addToGeneratedBy (index: number, generator: string) {
	if (lists[index].generatedBy.indexOf(generator) === -1) lists[index].generatedBy.push(generator);
}

/**
 * This function returns the index of the first list with a specific title in the array.
 * @param title The searched title.
 */
export function findListByTitle (title: string) {
	for (const index in lists) {
		if (lists[index].title === title) return Number(index);
	}
	return undefined;
}

/**
 * This functions asks the user to input a name for a new list.
 * @param index The index of the current list.
 */
export function askForListName (index: number): void {
	const input = prompt('Enter the name for the new list. Leave empty for no name.');

	if (input === null) return createList(index + 1);

	if (findListByTitle(input) !== undefined) {
		alert('This name exists!');
		return askForListName(index);
	}
	createList(index + 1, input);
}
