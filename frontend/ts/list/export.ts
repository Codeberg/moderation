import { lists } from '../main';
import { getColumnNames } from './index';

/**
 * This function exports a list to a CSV file.
 * @param index Index of the list that you want to export.
 */
export function exportList (index: number) {
	let content = '';
	// first row = column names
	const columnNames = getColumnNames(index);
	// add colon at the end to prevent import errors when column name is ``
	content += columnNames.join(';') + ';\n';
	for (const entries of lists[index].listRows) {
		for (const columnName of columnNames) {
			content += entries.has(columnName) ? entries.get(columnName) + ';' : ';';
		}
		content += '\n';
	}
	// create Blob and download CSV file
	const blob = new Blob([content], { type: 'text/csv' });
	const link = document.createElement('a');
	link.download = `cb_list_export-${ new Date().getTime() }${ lists[index].title ? '-' + lists[index].title : '' }.csv`;
	link.href = window.URL.createObjectURL(blob);
	link.click();
}
