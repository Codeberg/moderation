import { actionTabs as actionTabsDefinition } from '../actionDefinition';
import { lists } from '../main';

/**
 * This function opens a tab.
 * @param index Index of the tab.
 */
function selectTab (index: number) {
	// hide all tabs by removing the active class from tab buttons and panels
	document.querySelectorAll('[id^="cb-action-tab-"]').forEach((childNode) => {
		if (childNode instanceof HTMLButtonElement) childNode.classList.remove('active');
	});
	document.querySelectorAll('[id^="cb-action-panel-"]').forEach((childNode) => {
		if (childNode instanceof HTMLElement) childNode.classList.remove('active');
	});

	// show the desired tab by adding the active class to tab button and panel
	document.getElementById(`cb-action-tab-${ index }`)?.classList.add('active');
	document.getElementById(`cb-action-panel-${ index }`)?.classList.add('active');
}

// container for tab buttons
const actionTabContainer = document.getElementById('cb-list-action-tabs-container') as HTMLElement;
// container for panel elements
const actionPanelContainer = document.getElementById('cb-list-action-panels-container') as HTMLElement;

// preset for tab buttons
const actionTabPreset = document.createElement<'button'>('button');
actionTabPreset.type = 'button';
actionTabPreset.classList.add('cb-mod-tool--tab-button');

// preset for panel elements
const actionPanelPreset = document.createElement<'div'>('div');
actionPanelPreset.classList.add('cb-mod-tool--tab-panel');

/**
 * This function generates the tabs. It renders the tabs using the definition given in `actionDefinition.ts`.
 * @see /frontend/ts/actionDefinition.ts
 */
export function generateTabs () {
	// array to store the tab buttons
	const actionTabs: HTMLButtonElement[] = [];
	// array to store the panel elements
	const actionPanels: HTMLElement[] = [];

	// generate the tabs by iterating through the definition at index of the list type
	if (lists.length > 0 && lists[lists.length - 1].type !== undefined && actionTabsDefinition[lists[lists.length - 1].type!] !== undefined) {
		// get the Tab[] from the definition
		const definitions = actionTabsDefinition[lists[lists.length - 1].type!];
		// iterating the Tabs
		for (const index in definitions) {
			// create tab button
			const tab = actionTabPreset.cloneNode() as HTMLButtonElement;
			tab.textContent = definitions[index].title;
			tab.addEventListener('click', () => selectTab(Number(index)));
			tab.id = `cb-action-tab-${ index }`;
			actionTabs.push(tab);

			// create panel element
			const panel = actionPanelPreset.cloneNode() as HTMLElement;
			panel.id = `cb-action-panel-${ index }`;

			// add action elements to panel by iterating the Action[] of the Tab definition
			for (const input in definitions[index].actions) {
				// get the Action
				const action = definitions[index].actions[input];

				// generate all HTMLElements needed for the input
				const container = document.createElement<'div'>('div');
				let el: HTMLTextAreaElement | HTMLInputElement;
				const label = document.createElement<'label'>('label');

				// determining the input's type
				if (action.type === 'textarea') {
					el = document.createElement<'textarea'>('textarea');
				} else {
					el = document.createElement<'input'>('input');
					el.type = action.type;
				}

				// set attributes
				container.classList.add('form-group');
				el.classList.add('form-control');
				el.name = action.name;
				label.textContent = action.label;
				if (action.onclick !== undefined) el.addEventListener('click', action.onclick);

				// generating id
				let id = `cb-action-input-${ index }-${ input }`;

				if (action.attributes !== undefined) {
					if (action.attributes.has('id')) {
						id = action.attributes.get('id')!;
					}
					action.attributes.forEach((value, key) => {
						if (key !== 'id' && key !== 'class') {
							el.setAttribute(key, value);
						} else if (key === 'class') {
							el.classList.value = '';
							el.classList.add(value);
						}
					});
				}

				el.id = id;
				label.htmlFor = id;

				// append elements to container and append container to the panel
				container.append(label, el);

				panel.append(container);
			}

			actionPanels.push(panel);
		}
	}
	// replace in tab container
	actionTabContainer.replaceChildren(...actionTabs);

	// replace in panel container
	actionPanelContainer.replaceChildren(...actionPanels);

	// select the first tab
	selectTab(0);
}
