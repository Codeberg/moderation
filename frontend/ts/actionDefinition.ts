type HTMLInputElementTypes = 'button' | 'checkbox' | 'date' | 'datetime-local' | 'email' | 'file' | 'hidden' | 'month' | 'number' | 'password' | 'radio' | 'range' | 'tel' | 'time' | 'text' | 'url' | 'week' | 'textarea';
export type Action = {
	type: HTMLInputElementTypes
	name: string
	label: string
	onclick?: () => void
	attributes?: Map<string, string>
};

export type Tab = {
	title: string
	actions: Action[]
}

/**
 * This function converts an object to a Map.
 * @param obj
 */
// @ts-ignore
function objectToMap(obj: object) {
	return new Map<string, string>([...Object.entries(obj)]);
}

/**
 * Definition of the action tabs and inputs based on the list input type.
 */
export const actionTabs: { [dataType: string]: Tab[] } = {
	user: [
		{
			title: 'edit',
			actions: []
		},
		{
			title: 'contact',
			actions: [
				{
					type: 'text',
					name: 'subject',
					label: 'Subject'
				},
				{
					type: 'textarea',
					name: 'message',
					label: 'Message'
				}
			]
		}
	]
};
