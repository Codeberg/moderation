import { processInput } from './index';

// input field at top of page
const inputField = document.getElementById('cb-input-field') as HTMLInputElement;


// ASSIGN FUNCTIONS TO ELEMENTS
inputField.addEventListener('keypress', e => {
	// on enter new line
	if (e.code === 'Enter') {
		const rows = inputField.value.replace(/^(https:\/\/[^/]*|)\//gm, '').split('\n')
			.filter((line: any) => line !== '').length;
		inputField.setAttribute('rows', String(rows + 1 > 200 ? 200 : rows + 1));
	}
	// on CTRL + Enter process the input
	if (e.code === 'Enter' && e.ctrlKey) {
		processInput(inputField.value);
		// unset input field value
		inputField.value = '';
		inputField.setAttribute('rows', String(1));
	}
});

// simulate CTRL + Enter on button click
document.getElementById('cb-input-button')!.addEventListener('click', () => {
	inputField.dispatchEvent(new KeyboardEvent('keypress', {
		code: 'Enter',
		ctrlKey: true
	}));
});
