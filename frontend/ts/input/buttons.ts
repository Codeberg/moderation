import { inputTabs as inputTabsDefinition } from '../inputDefinition';
import { staticInputButtons } from '../inputDefinition';

/**
 * This function opens a tab.
 * @param index Index of the tab.
 */
function selectTab (index: number) {
	// hide all tabs by removing the active class from tab buttons and panels
	document.querySelectorAll('[id^="cb-input-tab-"]').forEach((childNode) => {
		if (childNode instanceof HTMLButtonElement) childNode.classList.remove('active');
	});
	document.querySelectorAll('[id^="cb-input-panel-"]').forEach((childNode) => {
		if (childNode instanceof HTMLElement) childNode.classList.remove('active');
	});

	// show the desired tab by adding the active class to tab button and panel
	document.getElementById(`cb-input-tab-${ index }`)?.classList.add('active');
	document.getElementById(`cb-input-panel-${ index }`)?.classList.add('active');
}

// container for tab buttons
const inputTabContainer = document.getElementById('cb-input-tabs-container') as HTMLElement;
// container for panel elements
const inputPanelContainer = document.getElementById('cb-input-panels-container') as HTMLElement;

// preset for buttons in the navigation section
const inputButtonPreset = document.getElementById('cb-input-button-placeholder') as HTMLButtonElement;
inputButtonPreset.removeAttribute('id');

// preset for tab buttons
const inputTabPreset = document.createElement<'button'>('button');
inputTabPreset.type = 'button';
inputTabPreset.classList.add('cb-mod-tool--tab-button');

// preset for panel elements
const inputPanelPreset = document.createElement<'div'>('div');
inputPanelPreset.classList.add('cb-mod-tool--tab-panel');

// array to store the buttons that will be statically displayed in the input section
const staticButtons: HTMLButtonElement[] = [];

// array to store the panel elements
const inputPanels: HTMLElement[] = [];
// array to store the tab buttons
const inputTabs: HTMLButtonElement[] = [];

/**
 * This function generates a button for the navigation section.
 * @param label Text content of the button.
 * @param onClick Function that will be executed on the `click` event.
 */
const generateButton = (label: string, onClick: () => void) => {
	// clone from preset
	const button = inputButtonPreset.cloneNode() as HTMLButtonElement;
	// set text content
	button.textContent = label;
	// add event listener
	button.addEventListener('click', onClick);
	return button;
};

// TODO: necessary? remove?
// generate buttons that will be statically displayed
for (const inputButtonLabel in staticInputButtons)
	staticButtons.push(generateButton(inputButtonLabel, staticInputButtons[inputButtonLabel]));
// replace preset with generated buttons
inputButtonPreset.replaceWith(...staticButtons);

// generate tabs by iterating through the definition (/frontend/ts/inputDefinition.ts)
/**
 * @see /frontend/ts/inputDefinition.ts
 */
for (const index in inputTabsDefinition) {
	// create tab button
	const tab = inputTabPreset.cloneNode() as HTMLButtonElement;
	tab.textContent = inputTabsDefinition[index].title;
	tab.addEventListener('click', () => selectTab(Number(index)));
	tab.id = `cb-input-tab-${ index }`;
	inputTabs.push(tab);

	// create panel element
	const panel = inputPanelPreset.cloneNode() as HTMLElement;
	panel.id = `cb-input-panel-${ index }`;

	// add buttons to panel
	for (const buttonLabel in inputTabsDefinition[index].buttons)
		panel.appendChild(generateButton(buttonLabel, inputTabsDefinition[index].buttons[buttonLabel]));
	inputPanels.push(panel);
}

// replace in tab container
inputTabContainer.replaceChildren(...inputTabs);

// replace in panel container
inputPanelContainer.replaceChildren(...inputPanels);

// select the first tab
selectTab(0);
